<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001623228162';
$dateexpire = '001659228162';
$ser_content = 'a:2:{s:7:"CONTENT";s:6278:"


<div class="items-list">
    <div class="item-content">
	
    <article id="bx_3218110189_4">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/ulitsa-ne-dlya-lyudey-meriya-parizha-vzyalas-prevratit-eliseyskie-polya-v-sad">Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/ulitsa-ne-dlya-lyudey-meriya-parizha-vzyalas-prevratit-eliseyskie-polya-v-sad"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/b68/b68cc3279429c36c4009063a365f1070.jpg"
                            width="417"
                            height="233"
                            alt="Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад"
                            title="Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад"
                        /></span></a>
                    
                    <p>В Париже запущен проект реконструкции Елисейских полей, за которую боролась инициативная группа жителей города с 2019 года — на улицу хотят вернуть парижан, которые сейчас избегают загазованной магистрали, посещаемой только туристами. К 2024 году здесь предполагается расширить пешеходную часть за счет проезжей, благоустроить зону отдыха и высадить деревья, чтобы Елисейские поля вновь обрели утраченную привлекательность для местных жителей.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_3">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/ukololsya-i-poekhal-gde-vaktsinatsiya-oblegchaet-zhizn-turistam">Укололся и поехал: где вакцинация облегчает жизнь туристам</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/ukololsya-i-poekhal-gde-vaktsinatsiya-oblegchaet-zhizn-turistam"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/f34/f3497a005cd5cfe146f4b45351d2b217.JPG"
                            width="414"
                            height="233"
                            alt="Укололся и поехал: где вакцинация облегчает жизнь туристам"
                            title="Укололся и поехал: где вакцинация облегчает жизнь туристам"
                        /></span></a>
                    
                    <p>Страны мира в условиях пандемии готовятся к летнему сезону отпусков – и постепенно объявляют, кого готовы принять. В список условий открытия въезда для туристов все чаще называют свидетельство о вакцинации против COVID-19. Пока что это не значит, что привившиеся россияне могут ехать в страны, открывающиеся или предлагающие облегченные условия въезда для вакцинированных. Список государств, где нас ждут, пополняется.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_2">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/idei-vmesto-stankov-kuda-poekhat-v-rossii-za-sovremennym-iskusstvom">Идеи вместо станков: куда поехать в России за современным искусством</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/idei-vmesto-stankov-kuda-poekhat-v-rossii-za-sovremennym-iskusstvom"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/499/49913e4619df1094493c95d11952ffe4.jpg"
                            width="414"
                            height="233"
                            alt="Идеи вместо станков: куда поехать в России за современным искусством"
                            title="Идеи вместо станков: куда поехать в России за современным искусством"
                        /></span></a>
                    
                    <p>Выставки современного искусства для российских туристов были одной из причин отправиться в страны Европы или в США. Сейчас это практически невозможно, поэтому «Газета.Ru» окинула взглядом пространства нашей родины и нашла, куда поехать ради красивых пространств в бывших промышленных зданиях и новых впечатлений.</p>
            </article>
    </div>
</div>
<div id="pag">
    

            
        <div class="load-more-items" data-url="/novosti/?PAGEN_1=3">Показать еще</div>

    
</div>


";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:3:{i:0;i:4;i:1;i:3;i:2;i:2;}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/bitrix/templates/shop/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:6:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"4";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"4";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"3";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"3";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"2";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"2";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>