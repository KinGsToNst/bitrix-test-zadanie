<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001623321394';
$dateexpire = '001659321394';
$ser_content = 'a:2:{s:7:"CONTENT";s:4450:"


<div class="items-list">
    <div class="item-content">
	
    <article id="bx_3218110189_7">
        <header>
                                                <h2><a href="/novosti/khaytek/tolpy-lyudey-zapolnili-berega-temzy-kak-v-londone-prazdnovali-pobedu-bez-sssr">«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/khaytek/tolpy-lyudey-zapolnili-berega-temzy-kak-v-londone-prazdnovali-pobedu-bez-sssr"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/36e/36e479851f871fbe97896d23a0e6974c.jpg"
                            width="417"
                            height="233"
                            alt="«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР"
                            title="«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР"
                        /></span></a>
                    
                    <p>8 июня 1946 года в Лондоне состоялся один из самых известных парадов Победы во Второй мировой войне. По британской столице прошагали воинские части из стран антигитлеровской коалиции со всего мира. По политическим мотивам в Лондон не приехали представители СССР и Югославии. Из-за скандала отказались от участия и поляки.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_6">
        <header>
                                                <h2><a href="/novosti/khaytek/dokopalis-do-molekul-uchenye-pridumali-kak-nayti-vnezemnuyu-zhizn">Докопались до молекул: ученые придумали, как найти внеземную жизнь</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/khaytek/dokopalis-do-molekul-uchenye-pridumali-kak-nayti-vnezemnuyu-zhizn"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/5c9/5c965c5f6d2617387024851c90076527.jpg"
                            width="414"
                            height="233"
                            alt="Докопались до молекул: ученые придумали, как найти внеземную жизнь"
                            title="Докопались до молекул: ученые придумали, как найти внеземную жизнь"
                        /></span></a>
                    
                    <p>Первый проект по поиску внеземной жизни был запущен еще в 1959 году и был направлен на улавливание радиосигналов от далеких цивилизаций. Сегодня усилия во многом сконцентрированы на поиске химических признаков жизни на фрагментах метеоритов и других небесных тел. До сих пор ученые не слишком преуспели в поиске инопланетной жизни, и одна из причин этого — недостаточное понимание, что именно можно считать ее признаками, а что — нет. В последние годы удалось открыть тысячи экзопланет, где потенциально могла бы существовать жизнь, поэтому вопрос того, как ее можно надежно обнаружить, стоит довольно остро.</p>
            </article>
    </div>
</div>
<div id="pag">
    
</div>


";s:4:"VARS";a:2:{s:8:"arResult";a:8:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:59:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"2021-06-08 23:11:30";s:12:"~TIMESTAMP_X";s:19:"2021-06-08 23:11:30";s:11:"MODIFIED_BY";s:1:"1";s:12:"~MODIFIED_BY";s:1:"1";s:11:"DATE_CREATE";s:19:"2021-06-08 23:11:30";s:12:"~DATE_CREATE";s:19:"2021-06-08 23:11:30";s:10:"CREATED_BY";s:1:"1";s:11:"~CREATED_BY";s:1:"1";s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:12:"Хайтек";s:5:"~NAME";s:12:"Хайтек";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"LEFT_MARGIN";s:1:"3";s:12:"~LEFT_MARGIN";s:1:"3";s:12:"RIGHT_MARGIN";s:1:"4";s:13:"~RIGHT_MARGIN";s:1:"4";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:18:"SEARCHABLE_CONTENT";s:14:"ХАЙТЕК
";s:19:"~SEARCHABLE_CONTENT";s:14:"ХАЙТЕК
";s:4:"CODE";s:7:"khaytek";s:5:"~CODE";s:7:"khaytek";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:14:"DETAIL_PICTURE";N;s:15:"~DETAIL_PICTURE";N;s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:16:"SECTION_PAGE_URL";s:17:"/novosti/khaytek/";s:17:"~SECTION_PAGE_URL";s:17:"/novosti/khaytek/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:16:"IPROPERTY_VALUES";a:0:{}}}}s:8:"ELEMENTS";a:2:{i:0;i:7;i:1;i:6;}s:16:"IPROPERTY_VALUES";a:0:{}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/bitrix/templates/shop/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:4:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"7";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"7";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"6";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"6";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>