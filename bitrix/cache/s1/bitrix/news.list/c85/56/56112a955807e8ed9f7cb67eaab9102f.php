<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001623228150';
$dateexpire = '001659228150';
$ser_content = 'a:2:{s:7:"CONTENT";s:7043:"


<div class="items-list">
    <div class="item-content">
	
    <article id="bx_3218110189_7">
        <header>
                                                <h2><a href="/novosti/khaytek/tolpy-lyudey-zapolnili-berega-temzy-kak-v-londone-prazdnovali-pobedu-bez-sssr">«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/khaytek/tolpy-lyudey-zapolnili-berega-temzy-kak-v-londone-prazdnovali-pobedu-bez-sssr"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/36e/36e479851f871fbe97896d23a0e6974c.jpg"
                            width="417"
                            height="233"
                            alt="«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР"
                            title="«Толпы людей заполнили берега Темзы»: как в Лондоне праздновали Победу без СССР"
                        /></span></a>
                    
                    <p>8 июня 1946 года в Лондоне состоялся один из самых известных парадов Победы во Второй мировой войне. По британской столице прошагали воинские части из стран антигитлеровской коалиции со всего мира. По политическим мотивам в Лондон не приехали представители СССР и Югославии. Из-за скандала отказались от участия и поляки.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_6">
        <header>
                                                <h2><a href="/novosti/khaytek/dokopalis-do-molekul-uchenye-pridumali-kak-nayti-vnezemnuyu-zhizn">Докопались до молекул: ученые придумали, как найти внеземную жизнь</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/khaytek/dokopalis-do-molekul-uchenye-pridumali-kak-nayti-vnezemnuyu-zhizn"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/5c9/5c965c5f6d2617387024851c90076527.jpg"
                            width="414"
                            height="233"
                            alt="Докопались до молекул: ученые придумали, как найти внеземную жизнь"
                            title="Докопались до молекул: ученые придумали, как найти внеземную жизнь"
                        /></span></a>
                    
                    <p>Первый проект по поиску внеземной жизни был запущен еще в 1959 году и был направлен на улавливание радиосигналов от далеких цивилизаций. Сегодня усилия во многом сконцентрированы на поиске химических признаков жизни на фрагментах метеоритов и других небесных тел. До сих пор ученые не слишком преуспели в поиске инопланетной жизни, и одна из причин этого — недостаточное понимание, что именно можно считать ее признаками, а что — нет. В последние годы удалось открыть тысячи экзопланет, где потенциально могла бы существовать жизнь, поэтому вопрос того, как ее можно надежно обнаружить, стоит довольно остро.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_5">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/pitatelnaya-eda-dlya-veganov-vypustili-yaytsa-rastitelnogo-proiskhozhdeniya">«Питательная еда»: для веганов выпустили яйца растительного происхождения</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/pitatelnaya-eda-dlya-veganov-vypustili-yaytsa-rastitelnogo-proiskhozhdeniya"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/8a2/8a2ebcdf3b302c5ce4f0e4bfdb75141c.jpg"
                            width="414"
                            height="233"
                            alt="«Питательная еда»: для веганов выпустили яйца растительного происхождения"
                            title="«Питательная еда»: для веганов выпустили яйца растительного происхождения"
                        /></span></a>
                    
                    <p>«Питательная еда»: для веганов выпустили яйца растительного происхождения<br />
Среди продуктов для веганов появились «вареные яйца» – их изобрела компания OsomeFood из Сингапура. Она уже продает фальшивые рыбные шарики и котлетки с полезными для здоровья витаминами и микроэлементами и безуглеводную лапшу. Теперь к этим продуктам добавились «яйца» из прессованного морковного сока, миндального молока, картофельного крахмала, морских водорослей и других растительных ингредиентов. Они продаются в замороженном виде, а на стол подаются холодными – например, в составе веганского супа «рамен» с лапшой.</p>
            </article>
    </div>
</div>
<div id="pag">
    

            
        <div class="load-more-items" data-url="/novosti/?PAGEN_1=2">Показать еще</div>

    
</div>


";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:3:{i:0;i:7;i:1;i:6;i:2;i:5;}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/bitrix/templates/shop/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:6:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"7";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"7";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"6";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"6";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"5";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"5";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>