<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001623321425';
$dateexpire = '001659321425';
$ser_content = 'a:2:{s:7:"CONTENT";s:3931:"


<div class="items-list">
    <div class="item-content">
	
    <article id="bx_3218110189_2">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/idei-vmesto-stankov-kuda-poekhat-v-rossii-za-sovremennym-iskusstvom">Идеи вместо станков: куда поехать в России за современным искусством</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/idei-vmesto-stankov-kuda-poekhat-v-rossii-za-sovremennym-iskusstvom"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/499/49913e4619df1094493c95d11952ffe4.jpg"
                            width="414"
                            height="233"
                            alt="Идеи вместо станков: куда поехать в России за современным искусством"
                            title="Идеи вместо станков: куда поехать в России за современным искусством"
                        /></span></a>
                    
                    <p>Выставки современного искусства для российских туристов были одной из причин отправиться в страны Европы или в США. Сейчас это практически невозможно, поэтому «Газета.Ru» окинула взглядом пространства нашей родины и нашла, куда поехать ради красивых пространств в бывших промышленных зданиях и новых впечатлений.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_1">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/ruki-proch-ot-kolizeya-chem-nedovolny-italyanskie-arkheologi">Руки прочь от Колизея: чем недовольны итальянские археологи</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/ruki-proch-ot-kolizeya-chem-nedovolny-italyanskie-arkheologi"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/cc8/cc8d73b7ede55ff09a73eed3a66c830d.jpg"
                            width="414"
                            height="233"
                            alt="Руки прочь от Колизея: чем недовольны итальянские археологи"
                            title="Руки прочь от Колизея: чем недовольны итальянские археологи"
                        /></span></a>
                    
                    <p>Итальянское правительство нашло исполнителя амбициозного проекта — воссоздание пола в римском Колизее, но в обновленном, высокотехнологичном виде. Но это не понравилось археологам и историкам искусства. Они считают, что министерство культуры хочет превратить древний амфитеатр в «дойную корову», а €15 млн, выделенные на это, можно потратить на восстановление разрушающихся церквей.</p>
            </article>
    </div>
</div>
<div id="pag">
    

    
</div>


";s:4:"VARS";a:2:{s:8:"arResult";a:8:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:59:{s:2:"ID";s:1:"2";s:3:"~ID";s:1:"2";s:11:"TIMESTAMP_X";s:19:"2021-06-08 23:11:59";s:12:"~TIMESTAMP_X";s:19:"2021-06-08 23:11:59";s:11:"MODIFIED_BY";s:1:"1";s:12:"~MODIFIED_BY";s:1:"1";s:11:"DATE_CREATE";s:19:"2021-06-08 23:11:59";s:12:"~DATE_CREATE";s:19:"2021-06-08 23:11:59";s:10:"CREATED_BY";s:1:"1";s:11:"~CREATED_BY";s:1:"1";s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:21:"Стиль жизни";s:5:"~NAME";s:21:"Стиль жизни";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"LEFT_MARGIN";s:1:"1";s:12:"~LEFT_MARGIN";s:1:"1";s:12:"RIGHT_MARGIN";s:1:"2";s:13:"~RIGHT_MARGIN";s:1:"2";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:18:"SEARCHABLE_CONTENT";s:23:"СТИЛЬ ЖИЗНИ
";s:19:"~SEARCHABLE_CONTENT";s:23:"СТИЛЬ ЖИЗНИ
";s:4:"CODE";s:11:"stil-zhizni";s:5:"~CODE";s:11:"stil-zhizni";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:14:"DETAIL_PICTURE";N;s:15:"~DETAIL_PICTURE";N;s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:16:"SECTION_PAGE_URL";s:21:"/novosti/stil-zhizni/";s:17:"~SECTION_PAGE_URL";s:21:"/novosti/stil-zhizni/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:16:"IPROPERTY_VALUES";a:0:{}}}}s:8:"ELEMENTS";a:2:{i:0;i:2;i:1;i:1;}s:16:"IPROPERTY_VALUES";a:0:{}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/bitrix/templates/shop/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:4:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"2";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"2";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"1";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"1";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>