<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001623321392';
$dateexpire = '001659321392';
$ser_content = 'a:2:{s:7:"CONTENT";s:6869:"


<div class="items-list">
    <div class="item-content">
	
    <article id="bx_3218110189_5">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/pitatelnaya-eda-dlya-veganov-vypustili-yaytsa-rastitelnogo-proiskhozhdeniya">«Питательная еда»: для веганов выпустили яйца растительного происхождения</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/pitatelnaya-eda-dlya-veganov-vypustili-yaytsa-rastitelnogo-proiskhozhdeniya"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/8a2/8a2ebcdf3b302c5ce4f0e4bfdb75141c.jpg"
                            width="414"
                            height="233"
                            alt="«Питательная еда»: для веганов выпустили яйца растительного происхождения"
                            title="«Питательная еда»: для веганов выпустили яйца растительного происхождения"
                        /></span></a>
                    
                    <p>«Питательная еда»: для веганов выпустили яйца растительного происхождения<br />
Среди продуктов для веганов появились «вареные яйца» – их изобрела компания OsomeFood из Сингапура. Она уже продает фальшивые рыбные шарики и котлетки с полезными для здоровья витаминами и микроэлементами и безуглеводную лапшу. Теперь к этим продуктам добавились «яйца» из прессованного морковного сока, миндального молока, картофельного крахмала, морских водорослей и других растительных ингредиентов. Они продаются в замороженном виде, а на стол подаются холодными – например, в составе веганского супа «рамен» с лапшой.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_4">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/ulitsa-ne-dlya-lyudey-meriya-parizha-vzyalas-prevratit-eliseyskie-polya-v-sad">Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/ulitsa-ne-dlya-lyudey-meriya-parizha-vzyalas-prevratit-eliseyskie-polya-v-sad"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/b68/b68cc3279429c36c4009063a365f1070.jpg"
                            width="417"
                            height="233"
                            alt="Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад"
                            title="Улица не для людей: мэрия Парижа взялась превратить Елисейские поля в сад"
                        /></span></a>
                    
                    <p>В Париже запущен проект реконструкции Елисейских полей, за которую боролась инициативная группа жителей города с 2019 года — на улицу хотят вернуть парижан, которые сейчас избегают загазованной магистрали, посещаемой только туристами. К 2024 году здесь предполагается расширить пешеходную часть за счет проезжей, благоустроить зону отдыха и высадить деревья, чтобы Елисейские поля вновь обрели утраченную привлекательность для местных жителей.</p>
            </article>
    </div>
    <div class="item-content">
	
    <article id="bx_3218110189_3">
        <header>
                                                <h2><a href="/novosti/stil-zhizni/ukololsya-i-poekhal-gde-vaktsinatsiya-oblegchaet-zhizn-turistam">Укололся и поехал: где вакцинация облегчает жизнь туристам</a></h2>
                            
                            <p>8 июня 2021</p>
            
        </header>
                                    <a href="/novosti/stil-zhizni/ukololsya-i-poekhal-gde-vaktsinatsiya-oblegchaet-zhizn-turistam"><span class="image featured"><img
                            class="preview_picture"
                            src="/upload/iblock/f34/f3497a005cd5cfe146f4b45351d2b217.JPG"
                            width="414"
                            height="233"
                            alt="Укололся и поехал: где вакцинация облегчает жизнь туристам"
                            title="Укололся и поехал: где вакцинация облегчает жизнь туристам"
                        /></span></a>
                    
                    <p>Страны мира в условиях пандемии готовятся к летнему сезону отпусков – и постепенно объявляют, кого готовы принять. В список условий открытия въезда для туристов все чаще называют свидетельство о вакцинации против COVID-19. Пока что это не значит, что привившиеся россияне могут ехать в страны, открывающиеся или предлагающие облегченные условия въезда для вакцинированных. Список государств, где нас ждут, пополняется.</p>
            </article>
    </div>
</div>
<div id="pag">
    

            
        <div class="load-more-items" data-url="/novosti/stil-zhizni/?PAGEN_1=2">Показать еще</div>

    
</div>


";s:4:"VARS";a:2:{s:8:"arResult";a:8:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:13:"LIST_PAGE_URL";s:47:"#SITE_DIR#/information/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:14:"Новости";s:7:"SECTION";a:1:{s:4:"PATH";a:1:{i:0;a:59:{s:2:"ID";s:1:"2";s:3:"~ID";s:1:"2";s:11:"TIMESTAMP_X";s:19:"2021-06-08 23:11:59";s:12:"~TIMESTAMP_X";s:19:"2021-06-08 23:11:59";s:11:"MODIFIED_BY";s:1:"1";s:12:"~MODIFIED_BY";s:1:"1";s:11:"DATE_CREATE";s:19:"2021-06-08 23:11:59";s:12:"~DATE_CREATE";s:19:"2021-06-08 23:11:59";s:10:"CREATED_BY";s:1:"1";s:11:"~CREATED_BY";s:1:"1";s:9:"IBLOCK_ID";s:1:"1";s:10:"~IBLOCK_ID";s:1:"1";s:17:"IBLOCK_SECTION_ID";N;s:18:"~IBLOCK_SECTION_ID";N;s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:13:"GLOBAL_ACTIVE";s:1:"Y";s:14:"~GLOBAL_ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:4:"NAME";s:21:"Стиль жизни";s:5:"~NAME";s:21:"Стиль жизни";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"LEFT_MARGIN";s:1:"1";s:12:"~LEFT_MARGIN";s:1:"1";s:12:"RIGHT_MARGIN";s:1:"2";s:13:"~RIGHT_MARGIN";s:1:"2";s:11:"DEPTH_LEVEL";s:1:"1";s:12:"~DEPTH_LEVEL";s:1:"1";s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:18:"SEARCHABLE_CONTENT";s:23:"СТИЛЬ ЖИЗНИ
";s:19:"~SEARCHABLE_CONTENT";s:23:"СТИЛЬ ЖИЗНИ
";s:4:"CODE";s:11:"stil-zhizni";s:5:"~CODE";s:11:"stil-zhizni";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:14:"DETAIL_PICTURE";N;s:15:"~DETAIL_PICTURE";N;s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:13:"LIST_PAGE_URL";s:9:"/novosti/";s:14:"~LIST_PAGE_URL";s:9:"/novosti/";s:16:"SECTION_PAGE_URL";s:21:"/novosti/stil-zhizni/";s:17:"~SECTION_PAGE_URL";s:21:"/novosti/stil-zhizni/";s:14:"IBLOCK_TYPE_ID";s:11:"information";s:15:"~IBLOCK_TYPE_ID";s:11:"information";s:11:"IBLOCK_CODE";s:4:"news";s:12:"~IBLOCK_CODE";s:4:"news";s:18:"IBLOCK_EXTERNAL_ID";N;s:19:"~IBLOCK_EXTERNAL_ID";N;s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:16:"IPROPERTY_VALUES";a:0:{}}}}s:8:"ELEMENTS";a:3:{i:0;i:5;i:1;i:4;i:2;i:3;}s:16:"IPROPERTY_VALUES";a:0:{}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:86:"/bitrix/templates/shop/components/bitrix/news/news/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:6:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"5";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"5";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"4";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"4";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:1:"3";i:2;N;i:3;s:31:"Изменить новость";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:1:"3";i:2;N;i:3;s:29:"Удалить новость";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>