<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <ul class="links">
        <?php
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <li class="current"><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
        <?else:?>
            <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
        <?endif?>

        <?endforeach?>

        <?/*<li><a href="#">Mattis et quis rutrum</a></li>
        <li><a href="#">Suspendisse amet varius</a></li>
        <li><a href="#">Sed et dapibus quis</a></li>
        <li><a href="#">Rutrum accumsan dolor</a></li>
        <li><a href="#">Mattis rutrum accumsan</a></li>
        <li><a href="#">Suspendisse varius nibh</a></li>
        <li><a href="#">Sed et dapibus mattis</a></li>*/?>
    </ul>




<?endif?>